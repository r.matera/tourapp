package it.uniba.di.sms.tourapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class InterestsFragment extends Fragment {

    ArrayList<Interest> interests;  //Arraylist of contacts/Elements of the list

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.menu_interests);

    }

    public InterestsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_interests, container, false);

        // RecyclerView variable declaration
        RecyclerView rvInterests = view.findViewById(R.id.rvInterests);

        // Initialize interests list
        interests = Interest.createInterestList();
        // Create adapter passing in the data
        InterestsAdapter adapter = new InterestsAdapter(interests);
        // Attach the adapter to the recyclerview to populate items
        rvInterests.setAdapter(adapter);
        // Set layout manager to position the items
        rvInterests.setLayoutManager(new LinearLayoutManager(getActivity().getBaseContext()));

        //Adds the line separator between items of the list
        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);

        rvInterests.addItemDecoration(itemDecoration);



        return view;
    }


}
