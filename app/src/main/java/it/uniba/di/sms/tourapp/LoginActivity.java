package it.uniba.di.sms.tourapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class LoginActivity extends AppCompatActivity {

    private EditText etUsername;
    private EditText etPassword;
    private Button btn_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        etUsername = findViewById(R.id.name_EditText);
        etPassword = findViewById(R.id.password_EditText);
        btn_login = findViewById(R.id.button_login);


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserLoginTask checkLogin = new UserLoginTask(etUsername.getText().toString(), etPassword.getText().toString());
                checkLogin.execute();
            }
        });

    }

    public class UserLoginTask extends AsyncTask<String, Void, String> {

        private final String mUsername;
        private final String mPassword;

        //costruttore UserLoginTask
        UserLoginTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection conn;
            URL url = null;

            try {

                // file php del database
                url = new URL("http://tourapp.altervista.org/loginUtente.php");

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                //classe HttpURLConnection per inviare e ricevere dati da file php
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");

                // metodi setDoInput e setDoOutput per la gestione dei dati inviati e ricevuti
                conn.setDoInput(true);
                conn.setDoOutput(true);

                String query = URLEncoder.encode("username","UTF-8")+"="+URLEncoder.encode(mUsername , "UTF-8")
                        +"&&"+URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(mPassword,"UTF-8");


                // si apre la connessione per ricevere i dati
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();


                // controllo se la connessione è stata fatta
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // lettura dei dati inviati dal server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    String result = "";
                    String line;


                    while ((line = reader.readLine()) != null) {
                        result += line;
                    }

                    // Passaggio dei dati al metodo onPostExecute
                    return result;

                } else {

                    return ("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } catch (NullPointerException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }

        }

        @Override
        protected void onPostExecute(String s) {
            boolean result = false;

            //controllo della correttezza dei dati sul login

            if (s != null) {
                result = new Boolean(s);
                if (result){

                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("username", mUsername);
                    editor.putString("password", mPassword);
                    editor.putBoolean("isLogged", true);
                    editor.commit();


                    Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                    startActivity(intent);
                    LoginActivity.this.finish();
                } else {
                    Toast.makeText(LoginActivity.this, R.string.login_error_invalid, Toast.LENGTH_LONG).show();
                }
            }
        }

    }
}


