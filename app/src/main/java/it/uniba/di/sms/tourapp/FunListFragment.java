package it.uniba.di.sms.tourapp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class FunListFragment extends Fragment {

    private static final String TAG = "Activity";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.interessi_svago);

    }

    public FunListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragments_list, container, false);


        Log.d(TAG, "onCreate: started.");

        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = view.findViewById(R.id.recyclerv_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), mNames, mImageUrls,mDescription);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        initImageBitmaps();
        initDescriptions();
        return view;
    }

    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mImageUrls.add("https://www.codiceabar.it/wp-content/uploads/2016/12/14876536_568790333304337_8747990691395711481_o.jpg");
        mNames.add(getString(R.string.first_fun_name));

        mImageUrls.add("http://www.kabukiclub.it/wp-content/uploads/2015/01/im4a.jpg");
        mNames.add(getString(R.string.second_fun_name));

        mImageUrls.add("https://d3nu7sbd9ucju9.cloudfront.net/articles/original/c14/5bb5e24893e8da6916d77a50.jpg/w=1024&h=-1&bri=0&high=0");
        mNames.add(getString(R.string.third_fun_name));

        mImageUrls.add("http://www.timeoutpub.it/images/1200x630.jpg");
        mNames.add(getString(R.string.forth_fun_name));

        mImageUrls.add("http://www.pubterradimezzo.it/img/slider/pub-terra-di-mezzo-slide-3.jpg");
        mNames.add(getString(R.string.fifth_fun_name));

        mImageUrls.add("https://d3nu7sbd9ucju9.cloudfront.net/articles/original/dde/5b88332293e8da69162ebb9f.jpg/w=1024&h=-1&bri=0&high=0");
        mNames.add(getString(R.string.sixt_fun_name));

        mImageUrls.add("https://d3nu7sbd9ucju9.cloudfront.net/articles/original/29e/5a57a55cd77f237ad9d96ccc.jpg/w=1024&h=-1&bri=0&high=0");
        mNames.add(getString(R.string.seventh_fun_name));

        mImageUrls.add("https://d3nu7sbd9ucju9.cloudfront.net/articles/original/667/5649b0da8fc825d5ace75705.jpg/w=1024&h=-1&bri=0&high=0");
        mNames.add(getString(R.string.eighth_fun_name));

    }

    private void initDescriptions(){
        mDescription.add(getString(R.string.first_fun_description));
        mDescription.add(getString(R.string.second_fun_description));
        mDescription.add(getString(R.string.third_fun_description));
        mDescription.add(getString(R.string.forth_fun_description));
        mDescription.add(getString(R.string.fifth_fun_description));

        mDescription.add(getString(R.string.sixth_fun_description));
        mDescription.add(getString(R.string.seventh_fun_description));
        mDescription.add(getString(R.string.eighth_fun_description));
    }



}
