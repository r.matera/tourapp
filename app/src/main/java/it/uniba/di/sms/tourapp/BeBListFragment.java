package it.uniba.di.sms.tourapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class BeBListFragment extends Fragment {

    private static final String TAG = "Activity";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.dormire_bb);

    }

    public BeBListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragments_list, container, false);


        Log.d(TAG, "onCreate: started.");

        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = view.findViewById(R.id.recyclerv_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), mNames, mImageUrls, mDescription);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        initImageBitmaps();
        initDescriptions();
        return view;
    }

    private void initImageBitmaps() {
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mImageUrls.add("https://scontent-fco1-1.xx.fbcdn.net/v/t1.0-9/62524230_2363817113907357_1480491363123331072_n.jpg?_nc_cat=109&_nc_oc=AQkLJCYcXclmqXH8DlZfa-l8Qpwc87mgja-ESkLhcISXMPtDF7l4FtGwboUAHJWu6cc&_nc_ht=scontent-fco1-1.xx&oh=5db760e53eb8f7cae3933094aa018b94&oe=5E1B4F7E");
        mNames.add(getString(R.string.bb_name1));

        mImageUrls.add("https://www.melobari.it/wp-content/uploads/2019/09/Melo-Accommodations-Bari-B_B-6.FOTO-PRINCIPALE-605x465.jpg");
        mNames.add(getString(R.string.bb_name2));

        mImageUrls.add("https://q-cf.bstatic.com/images/hotel/max1024x768/179/179388312.jpg");
        mNames.add(getString(R.string.bb_name3));


    }

    private void initDescriptions() {
        mDescription.add((getString(R.string.desc_bb1)));

        mDescription.add((getString(R.string.desc_bb2)));

        mDescription.add((getString(R.string.desc_bb3)));

    }

}
