package it.uniba.di.sms.tourapp;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MemoryDetailActivity extends AppCompatActivity {

    Memory memory;
    ArrayList<Memory> mList;

    int id_photo;
    String titolo;
    String descrizione;
    String data;

    // Riferimenti all'immageview,foto, titolo, descrizione, data (tutto del ricordo)
    ImageView fotoDettaglioRicordo;
    TextView textTitolo;
    TextView textDescrizione;
    TextView textData;
    Button btn_eliminaRicordo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory_detail);

        mList = new ArrayList<>();
        fotoDettaglioRicordo = findViewById(R.id.detail_fotoRicordo);
        textTitolo = findViewById(R.id.detail_titolo);
        textDescrizione = findViewById(R.id.detail_descrizione);
        textData = findViewById(R.id.detail_data);
        btn_eliminaRicordo = findViewById(R.id.btn_eliminaRicordo);


        // add back arrow to toolbar and setting the title
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }

        //Ottengo i parametri del ricordo cliccato nell'activity MemoryListActivity
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            ;
        } else {
            id_photo = extras.getInt("photo");
            titolo = extras.getString("titolo");
            descrizione = extras.getString("descrizione");
            data = extras.getString("data");

        }

        final SQLiteHelper sqLiteHelper = new SQLiteHelper(getApplicationContext());
        Cursor cursor = sqLiteHelper.getData("SELECT * FROM RICORDI");
        int i = 0;
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

            int id = cursor.getInt(0);
            String titolo = cursor.getString(1);
            String descrizione = cursor.getString(2);
            String data = cursor.getString(3);
            byte[] image = cursor.getBlob(4);

            // Aggiunta dell'oggetto memoria alla lista di memorie
            mList.add(new Memory(id, titolo, descrizione, data, image));

            //Controllo per la stampa del del dettaglio
            /* Se l'id della foto cliccata, passato mediante extra è uguale alla posizione
            del cursore nell'iterazione allora vengono settate le view.
             */
            if (id_photo == cursor.getPosition()) {

                byte[] memoryImage = mList.get(i).getImage();
                Bitmap bitmap = BitmapFactory.decodeByteArray(memoryImage, 0, memoryImage.length);
                fotoDettaglioRicordo.setImageBitmap(bitmap);
                textTitolo.setText(titolo);
                textData.setText(data);
                textDescrizione.setText(descrizione);
            }
            i++;
        }

        btn_eliminaRicordo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final SQLiteHelper sqLiteHelper = new SQLiteHelper(getApplicationContext());
                Cursor c = sqLiteHelper.getData("SELECT id FROM RICORDI");
                ArrayList<Integer> arrID = new ArrayList<Integer>();
                while (c.moveToNext()) {
                    arrID.add(c.getInt(0));
                }
                showDialogDelete(arrID.get(id_photo));
            }
        });


    }

    private void showDialogDelete(final int id_memory) {
        AlertDialog.Builder dialogDelete = new AlertDialog.Builder(MemoryDetailActivity.this);
        dialogDelete.setTitle(R.string.warning);
        dialogDelete.setMessage(R.string.confirm_delate_memory_message);
        dialogDelete.setPositiveButton(R.string.ok_dialog, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    final SQLiteHelper sqLiteHelper = new SQLiteHelper(getApplicationContext());
                    sqLiteHelper.deleteData(id_memory);
                    Toast.makeText(MemoryDetailActivity.this, getString(R.string.delate_text), Toast.LENGTH_SHORT).show();
                    finish();

                } catch (Exception e) {
                    Log.e("error", e.getMessage());
                }
            }
        });
        dialogDelete.setNegativeButton(R.string.back_dialog, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialogDelete.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

