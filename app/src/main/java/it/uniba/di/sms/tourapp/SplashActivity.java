package it.uniba.di.sms.tourapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final Boolean isLogged = sharedPref.getBoolean("isLogged", false);

        Thread timerThread = new Thread() {
            public void run(){
                try{
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (isLogged == true) {
                        Intent logged_intent = new Intent(SplashActivity.this, MenuActivity.class);
                        startActivity(logged_intent);
                    } else {
                        Intent not_logged_intent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(not_logged_intent);

                    }
                }
            }
        };
        timerThread.start();

    }
}