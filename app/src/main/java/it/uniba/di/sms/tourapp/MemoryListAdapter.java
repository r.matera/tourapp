package it.uniba.di.sms.tourapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MemoryListAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<Memory> memoryList;

    public MemoryListAdapter(Context context, int layout, ArrayList<Memory> memoryList) {
        this.context = context;
        this.layout = layout;
        this.memoryList = memoryList;
    }

    @Override
    public int getCount() {
        return memoryList.size();
    }

    @Override
    public Object getItem(int i) {
        return memoryList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class ViewHolder {
        ImageView imageView;
        TextView txtTitolo, txtData;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View row = view;
        ViewHolder holder = new ViewHolder();

        if (row == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout,null);
            holder.txtTitolo = row.findViewById(R.id.txtTitolo);
            holder.txtData = row.findViewById(R.id.txtData);
            holder.imageView = row.findViewById(R.id.imgIcon);

            row.setTag(holder);
        } else {
            holder = (ViewHolder)row.getTag();
        }

        Memory memory = memoryList.get(i);

        holder.txtTitolo.setText(memory.getTitolo());
        holder.txtData.setText(memory.getData());

        byte[] memoryImage = memory.getImage();
        Bitmap bitmap = BitmapFactory.decodeByteArray(memoryImage, 0, memoryImage.length);
        holder.imageView.setImageBitmap(bitmap);


        return row;
    }
}
