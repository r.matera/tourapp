package it.uniba.di.sms.tourapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_CANCELED;


public class DiaryFragment extends Fragment {

    EditText mEdtTitolo, mEdtDescrizione, mEdtData;
    Button mBtnAdd, mBtnList;
    ImageView mImageView;

    private String path;

    private static final String IMAGE_DIRECTORY = "/FotoTourApp";
    private int GALLERY = 1, CAMERA = 2;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 7;

    public static SQLiteHelper sqLiteHelper;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.menu_diary);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate del layout
        View view = inflater.inflate(R.layout.fragment_diary, container, false);

        mEdtTitolo = view.findViewById(R.id.edtTitle);
        mEdtDescrizione = view.findViewById(R.id.edtDetail);
        mEdtData = view.findViewById(R.id.edtDate);
        mBtnAdd = view.findViewById(R.id.btnAdd);
        mBtnList = view.findViewById(R.id.btnList);
        mImageView = view.findViewById(R.id.imageView);


        sqLiteHelper = new SQLiteHelper(getContext());

        //click sull'imageview per selezionare la foto
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    checkAndRequestPermissions();
                } else {
                    showPictureDialog();
                }
            }
        });

        //aggiunta record su sqlite
        mBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    sqLiteHelper.insertData(
                            mEdtTitolo.getText().toString().trim(),
                            mEdtDescrizione.getText().toString().trim(),
                            mEdtData.getText().toString().trim(),
                            imageViewToByte(mImageView)


                    );
                    Toast.makeText(getContext(), getString(R.string.memory_added), Toast.LENGTH_SHORT).show();
                    mEdtTitolo.setText("");
                    mEdtDescrizione.setText("");
                    mEdtData.setText("");
                    mImageView.setImageResource(R.drawable.ic_add_photo);

                    File imageFile = new File(path);
                    if (imageFile.exists()) {
                        ImageView myImage = new ImageView(getContext());
                        myImage.setImageURI(Uri.fromFile(imageFile));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //click sul bottone per visualizzare la lista dei ricordi
        mBtnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent list_intent = new Intent(getContext(), MemoryListActivity.class);
                startActivity(list_intent);
            }
        });


        return view;

    }


    public byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);


        byte[] byteArray = bytes.toByteArray();


        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // fare in modo che l'oggetto costruisca la struttura della directory, se necessario
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(byteArray);
            MediaScannerConnection.scanFile(getContext(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            path = f.getPath();
            fo.close();
            Log.e("TAG", "File Saved::--->" + f.getAbsolutePath());

        } catch (IOException e1) {
            e1.printStackTrace();

        }

        return byteArray;
    }


    //permessi per accedere alla fotocamera e alla galleria
    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
        int read = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        showPictureDialog();
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                // Inizializzare la mappa per i permessi
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Riempi con i risultati reali dell'utente
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Controllo per entrambi i permessi
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("in fragment on request", "CAMERA & READ_EXTERNAL_STORAGE permission granted");
                        //se i permessi non sono garantiti
                    } else {
                        Toast.makeText(getContext(), getString(R.string.memory_permessionNotGranted), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }
    }


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle(getString(R.string.memory_dialog1));
        String[] pictureDialogItems = {
                getString(R.string.memory_dialog_gallery),
                getString(R.string.memory_dialog_camera)};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallery();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), contentURI);
                    mImageView.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), getString(R.string.memory_failed), Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            mImageView.setImageBitmap(thumbnail);

        }
    }


}
