package it.uniba.di.sms.tourapp;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ChurchListFragment extends Fragment {

    private static final String TAG = "Activity";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.interessi_chiese);

    }

    public ChurchListFragment() {
        // Richiesto costruttore vuoto
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate del layout per questo fragment
        View view = inflater.inflate(R.layout.fragments_list, container, false);


        RecyclerView recyclerView = view.findViewById(R.id.recyclerv_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), mNames, mImageUrls,mDescription);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        initImageBitmaps();
        initDescriptions();
        return view;
    }

    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mImageUrls.add("https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Bari_BW_2016-10-19_13-35-11_stitch.jpg/1024px-Bari_BW_2016-10-19_13-35-11_stitch.jpg");
        mNames.add(getString(R.string.first_church_name));

        mImageUrls.add("http://webservice.around.bari.it/get.php?i=9c30391c");
        mNames.add(getString(R.string.second_church_name));

        mImageUrls.add("https://corrieredelmezzogiorno.corriere.it/methode_image/2018/04/26/Puglia/Foto%20Trattate/4606.0.788314676-kMO-U46060791416683s7H-1224x916@CorriereMezzogiorno-Web-Mezzogiorno.jpg?v=20180426124936");
        mNames.add(getString(R.string.third_church_name));

        mImageUrls.add("https://www.barinedita.it/timthumb.php?src=/public/foto_news_upload/san%20marco%20dei%20veneziani.jpg&w=480");
        mNames.add(getString(R.string.forth_church_name));

        mImageUrls.add("https://www.beweb.chiesacattolica.it/VisualizzaImmagine.do?sercd=41349&tipo=M5&fixedside=width&prog=1&statoinv=C&contesto=chiese");
        mNames.add(getString(R.string.fifth_church_name));

    }

    private void initDescriptions(){
        mDescription.add(getString(R.string.first_church_description));
        mDescription.add(getString(R.string.second_church_description));
        mDescription.add(getString(R.string.third_church_description));
        mDescription.add(getString(R.string.forth_church_description));
        mDescription.add(getString(R.string.fifth_church_description));
    }

}
