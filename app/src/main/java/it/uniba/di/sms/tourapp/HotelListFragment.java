package it.uniba.di.sms.tourapp;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class HotelListFragment extends Fragment {

    private static final String TAG = "Activity";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.dormire_hotel);

    }

    public HotelListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragments_list, container, false);


        Log.d(TAG, "onCreate: started.");

        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = view.findViewById(R.id.recyclerv_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), mNames, mImageUrls, mDescription);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        initImageBitmaps();
        initDescriptions();
        return view;
    }

    private void initImageBitmaps() {
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mImageUrls.add("https://www.meetingecongressi.com/Public/Fotografie/135/foto-7-the_nicolaus_hotel_135.jpg");
        mNames.add(getString(R.string.hotel_name1));

        mImageUrls.add("https://www.piazzadispagnaview.it/gallery/lrg/HOME_2_1.jpg");
        mNames.add(getString(R.string.hotel_name2));

        mImageUrls.add("https://bl24-bitrecallsrl.netdna-ssl.com/wp-content/uploads/2016/10/nazioni.jpg");
        mNames.add(getString(R.string.hotel_name3));


    }

    private void initDescriptions() {
        mDescription.add((getString(R.string.desc_hotel1)));

        mDescription.add((getString(R.string.desc_hotel2)));


        mDescription.add((getString(R.string.desc_hotel3)));


    }

}

