package it.uniba.di.sms.tourapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

public class SQLiteHelper extends SQLiteOpenHelper {

    // Nome del DB e versione
    final private static String NOME = "RICORDIDB.sqlite";
    final private static int VERSIONE = 1;

    //query per la creazione della tabella
    //creazione table in db
    final private static String crea_query = "CREATE TABLE IF NOT EXISTS RICORDI(id INTEGER PRIMARY KEY AUTOINCREMENT, titolo VARCHAR, descrizione VARCHAR, data VARCHAR, image BLOB);";


    //costruttore
    SQLiteHelper(Context context) {
        super(context, NOME, null, VERSIONE);
    }

    public void queryData(String sql) {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(sql);
    }


    public Cursor getData(String sql) {
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql, null);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        if (database.isOpen()) {

            //esecuzione query creazione tabella
            database.execSQL(crea_query);

        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    //insert data
    public void insertData(String titolo, String descrizione, String data, byte[] image) {
        SQLiteDatabase database = getWritableDatabase();
        //query per inserire record nel database
        String sql = "INSERT INTO RICORDI VALUES (NULL, ?, ?, ?, ?) ";

        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1, titolo);
        statement.bindString(2, descrizione);
        statement.bindString(3, data);
        statement.bindBlob(4, image);

        statement.executeInsert();

    }



    //updateData
    public void updateData (String titolo, String descrizione, String data, byte[] image, int id) {
        SQLiteDatabase database = getWritableDatabase();
        //query per aggiornare
        String sql = "UPDATE RICORDI SET titolo=?, descrizione=?, data=?, image=? WHERE id=?";

        SQLiteStatement statement = database.compileStatement(sql);

        statement.bindString(1, titolo);
        statement.bindString(2, descrizione);
        statement.bindString(3, data);
        statement.bindBlob(4, image);
        statement.bindDouble(5, (double)id);

        statement.execute();
        database.close();

    }

    //deleteData
    public void deleteData(int id) {
        SQLiteDatabase database = getWritableDatabase();

        String sql ="DELETE FROM RICORDI WHERE id=?";

        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);

        statement.execute();
        database.close();

    }



}
