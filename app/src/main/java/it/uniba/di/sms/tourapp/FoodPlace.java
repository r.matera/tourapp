package it.uniba.di.sms.tourapp;
import java.util.ArrayList;

public class FoodPlace {

    private int mIcon;
    private int mName;

    public FoodPlace(int icon, int name) { mIcon = icon; mName = name; }

    public int getIcon() {return mIcon; }
    public int getName() {
        return mName;
    }

    private static int lastId = 0;

    public static ArrayList<FoodPlace> createFoodPlacesList() {
        ArrayList<FoodPlace> foodPlaces = new ArrayList<FoodPlace>();

        foodPlaces.add(new FoodPlace(R.drawable.ic_restaurant, R.string.mangiare_ristoranti));
        foodPlaces.add(new FoodPlace(R.drawable.ic_pizza, R.string.mangiare_pizzerie));
        foodPlaces.add(new FoodPlace(R.drawable.ic_ice_cream, R.string.mangiare_gelaterie));


        return foodPlaces;
    }

}

