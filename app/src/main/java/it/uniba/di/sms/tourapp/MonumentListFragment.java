package it.uniba.di.sms.tourapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class MonumentListFragment extends Fragment {

    private static final String TAG = "Activity";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.interessi_monumenti);

    }

    public MonumentListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragments_list, container, false);


        Log.d(TAG, "onCreate: started.");

        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = view.findViewById(R.id.recyclerv_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), mNames, mImageUrls,mDescription);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        initImageBitmaps();
        initDescriptions();
        return view;
    }

    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mImageUrls.add("http://webservice.around.bari.it/get.php?i=7eeaaa69");
        mNames.add(getString(R.string.first_monument_name));

        mImageUrls.add("https://upload.wikimedia.org/wikipedia/commons/c/c8/Petruzzellibarioggi.jpg");
        mNames.add(getString(R.string.second_monument_name));

        mImageUrls.add("http://webservice.around.bari.it/get.php?i=d2999bb2");
        mNames.add(getString(R.string.third_monument_name));

        mImageUrls.add("http://www.visaresidence.com/wp-content/uploads/2017/06/nderrlalanz1-1024x576-1024x576.jpg");
        mNames.add(getString(R.string.forth_monument_name));

    }

    private void initDescriptions(){
        mDescription.add(getString(R.string.first_monument_description));
        mDescription.add(getString(R.string.second_monument_description));
        mDescription.add(getString(R.string.third_monument_description));
        mDescription.add(getString(R.string.forth_monument_description));

    }


}
