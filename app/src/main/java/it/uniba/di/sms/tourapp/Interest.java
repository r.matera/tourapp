package it.uniba.di.sms.tourapp;
import java.util.ArrayList;

public class Interest {

    private int mIcon;
    private int mName;

    public Interest(int icon, int name) { mIcon = icon; mName = name; }

    public int getIcon() {return mIcon;}
    public int getName() {
        return mName;
    }


    public static ArrayList<Interest> createInterestList() {
        ArrayList<Interest> interests = new ArrayList<Interest>();

        interests.add(new Interest(R.drawable.ic_monument, R.string.interessi_monumenti));
        interests.add(new Interest(R.drawable.ic_church, R.string.interessi_chiese));
        interests.add(new Interest(R.drawable.ic_beach, R.string.interessi_lidi));
        interests.add(new Interest(R.drawable.ic_wheel,R.string.interessi_svago));
        interests.add(new Interest(R.drawable.ic_locali, R.string.interessi_locali));




        return interests;
    }

}

