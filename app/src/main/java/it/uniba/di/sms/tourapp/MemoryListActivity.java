package it.uniba.di.sms.tourapp;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MemoryListActivity extends AppCompatActivity {

    ListView mListView;
    ArrayList<Memory> mList;
    MemoryListAdapter mAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory_list);

        mListView = findViewById(R.id.listView);
        mList = new ArrayList<>();
        mAdapter = new MemoryListAdapter(this, R.layout.memory_item, mList);
        mListView.setAdapter(mAdapter);

        // aggiunta freccia alla toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }

        //prendere dati da sqlite
        SQLiteHelper sqLiteHelper = new SQLiteHelper(getApplicationContext());
        Cursor cursor = sqLiteHelper.getData("SELECT * FROM RICORDI");
        mList.clear();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String titolo = cursor.getString(1);
            String descrizione = cursor.getString(2);
            String data = cursor.getString(3);
            byte[] image = cursor.getBlob(4);

            //aggiunta alla lista
            mList.add(new Memory(id, titolo, descrizione, data, image));
        }
        mAdapter.notifyDataSetChanged();
        if (mList.size() == 0) {
            //se non ci sono record nel table del db la listview è vuota
            Toast.makeText(this, getString(R.string.list_noFound), Toast.LENGTH_SHORT).show();
        }


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent memory_detail = new Intent(MemoryListActivity.this, MemoryDetailActivity.class);
                memory_detail.putExtra("photo", i);
                memory_detail.putExtra("titolo", mList.get(i).getTitolo());
                memory_detail.putExtra("descrizione" , mList.get(i).getDescrizione());
                memory_detail.putExtra("data", mList.get(i).getData());
                startActivity(memory_detail);
            }
        });


    }

    @Override
    public void onResume(){
        super.onResume();
        SQLiteHelper sqLiteHelper = new SQLiteHelper(getApplicationContext());
        Cursor cursor = sqLiteHelper.getData("SELECT * FROM RICORDI");
        mList.clear();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String titolo = cursor.getString(1);
            String descrizione = cursor.getString(2);
            String data = cursor.getString(3);
            byte[] image = cursor.getBlob(4);

            //aggiunta alla lista
            mList.add(new Memory(id,titolo,descrizione,data,image));
        }
        mAdapter.notifyDataSetChanged();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
