package it.uniba.di.sms.tourapp;
import java.util.ArrayList;

public class SleepPlaces {

    private int mIcon;
    private int mName;

    public SleepPlaces(int icon, int name) { mIcon = icon; mName = name; }

    public int getIcon() {return mIcon; }
    public int getName() {
        return mName;
    }

    private static int lastId = 0;

    public static ArrayList<SleepPlaces> createHotelsList() {
        ArrayList<SleepPlaces> sleepPlaces = new ArrayList<SleepPlaces>();

        sleepPlaces.add(new SleepPlaces(R.drawable.ic_hotel, R.string.dormire_hotel));
        sleepPlaces.add(new SleepPlaces(R.drawable.ic_bed_and_breakfast, R.string.dormire_bb));

        return sleepPlaces;
    }

}

