package it.uniba.di.sms.tourapp;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class EventsFragment extends Fragment {

    //Una lista per memorizzare gli eventi
    List<Event> eventList;

    //recyclerview
    RecyclerView recyclerView;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.menu_event);

    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events, container, false);

        //collegamento con il rw dell'xml
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //Inizializzzione della lista di eventi
        eventList = new ArrayList<>();


        //adding some items to our list
        eventList.add(
                new Event(
                        0,
                        getString(R.string.first_event_title),
                        getString(R.string.first_event_description),
                        R.drawable.baribrasilfilmfest));

        eventList.add(
                new Event(
                        1,
                        getString(R.string.second_event_title),
                        getString(R.string.second_event_description),
                        R.drawable.viniciocapossela));

        eventList.add(
                new Event(
                        2,
                        getString(R.string.third_event_title),
                        getString(R.string.third_event_description),
                        R.drawable.copernicushackanton));
        eventList.add(
                new Event(
                        3,
                        getString(R.string.forth_event_title),
                        getString(R.string.forth_event_description),
                        R.drawable.bariaraba));


        eventList.add(
                new Event(
                        4,
                        getString(R.string.fifth_event_title),
                        getString(R.string.fifth_event_description),
                        R.drawable.relazionitossiche));

        //creazione del recyclerview adapter
        EventAdapter adapter = new EventAdapter(getActivity(), eventList);

        //set dell'adapter
        recyclerView.setAdapter(adapter);



        return view;
    }
}

