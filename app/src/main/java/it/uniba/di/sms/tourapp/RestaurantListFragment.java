package it.uniba.di.sms.tourapp;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class RestaurantListFragment extends Fragment {

    private static final String TAG = "Activity";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.mangiare_ristoranti);

    }

    public RestaurantListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragments_list, container, false);


        Log.d(TAG, "onCreate: started.");

        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = view.findViewById(R.id.recyclerv_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), mNames, mImageUrls,mDescription);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        initImageBitmaps();
        initDescriptions();
        return view;
    }

    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mImageUrls.add("https://menu.sluurpy.it/foto-piatti/3482/3930.jpg");
        mNames.add(getString(R.string.restaurant_name1));

        mImageUrls.add("https://1.bp.blogspot.com/-SmHByOJgOHA/XMmJvVlk0bI/AAAAAAAAOWQ/YSkkVa9rnCEbKMRKcyoLNIL8gWotXjOEwCEwYBhgL/s1600/IMG_4322.jpeg");
        mNames.add(getString(R.string.restaurant_name2));

        mImageUrls.add("https://u.tfstatic.com/restaurant_photos/149/335149/169/612/osteria-villari-sala-a2f05.jpg");
        mNames.add(getString(R.string.restaurant_name3));


    }

    private void initDescriptions(){
        mDescription.add((getString(R.string.desc_rest1)));

        mDescription.add((getString(R.string.desc_rest2)));


        mDescription.add((getString(R.string.desc_rest3)));


    }

}
