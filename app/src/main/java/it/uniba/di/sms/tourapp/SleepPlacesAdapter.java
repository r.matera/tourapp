package it.uniba.di.sms.tourapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

// Creazione di adapter basico che estende RecyclerView.Adapter
//Il ViewHolder darà accesso alle nostre view
public class SleepPlacesAdapter extends
        RecyclerView.Adapter<SleepPlacesAdapter.ViewHolder> {


    // Fornisce un riferimento diretto a ciascuna delle viste all'interno di un elemento
    // Utilizzato per memorizzare nella cache le viste nel layout dell'elemento per un accesso rapido
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // L' holder contiene una variabile membro per ogni view che verrà settata
        // quando si fa il render della riga
        public TextView nameTextView;
        public ImageView imageView;
        private Context context;



        public ViewHolder(Context context, View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.item_hotel_icon);
            this.nameTextView = (TextView) itemView.findViewById(R.id.hotel_name);
            // memorizza il contesto
            this.context = context;
            // Attacca un click listener all'intera view
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // prende la poszione degli item
            if (position != RecyclerView.NO_POSITION) { // controlla se l'item è stato eliminato, ma l'utente lo ha cliccato prima che la UI lo rimuovesse
                // Possiamo accedere ai dati all'interno delle viste
                Toast.makeText(context, nameTextView.getText(), Toast.LENGTH_SHORT).show();
                /* Applicazione dello swich in base alla posizione dell'elemento cliccato per indirizzare
                    il flusso di navigazione verso il fragment desiderato. Lancia eccezione in caso di errore.
                 */
                AppCompatActivity activity = (AppCompatActivity) view.getContext();

                switch (position){
                    case 0:
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HotelListFragment()).addToBackStack(null).commit();
                        break;

                    case 1:
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new BeBListFragment()).addToBackStack(null).commit();
                        break;

                    default:

                        throw new IllegalStateException("Unexpected value: " + position);
                }

            }


        }
    }


    // variabile membro per i posti in cui dormire
    private List<SleepPlaces> mSleepPlaces;

    // Passaggio nel costruttore
    public SleepPlacesAdapter(List<SleepPlaces> sleepPlaces) {
        mSleepPlaces = sleepPlaces;
    }


    // inflate del layout e ritorno dell'holder
    @Override
    public SleepPlacesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate layout
        View contactView = inflater.inflate(R.layout.item_sleep_places, parent, false);

        // Ritorno di una nuova istanza di holder
        ViewHolder viewHolder = new ViewHolder( context, contactView);
        return viewHolder;
    }

    // popolamento dati negli item attraverso l'holder
    @Override
    public void onBindViewHolder(SleepPlacesAdapter.ViewHolder viewHolder, int position) {
        // prende i dati basandosi sulla posizione
        SleepPlaces sleepPlaces = mSleepPlaces.get(position);

        TextView textView = viewHolder.nameTextView;
        textView.setText(sleepPlaces.getName());

        ImageView imgView = viewHolder.imageView;
        imgView.setImageResource(sleepPlaces.getIcon());



    }


    // Restituisce il  totale degli item nella lista
    @Override
    public int getItemCount() {
        return mSleepPlaces.size();
    }


}
