package it.uniba.di.sms.tourapp;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class FoodPlacesFragment extends Fragment {

    ArrayList<FoodPlace> foodPlaces;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.menu_eat);

    }

    public FoodPlacesFragment() {
        // costruttore richiesto
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate layout
        View view = inflater.inflate(R.layout.fragment_food_places, container, false);

        // dichiarazione variabile RecyclerView
        RecyclerView rvFoodPlaces = view.findViewById(R.id.rvFoodPlaces);

        // Inizializzazione della lista
        foodPlaces = FoodPlace.createFoodPlacesList();
        // Creazione adapter
        FoodPlacesAdapter adapter = new FoodPlacesAdapter(foodPlaces);
        // Lego adapter alla recyclerView per popolare gli item
        rvFoodPlaces.setAdapter(adapter);
        // Set layout per posizionare gli item
        rvFoodPlaces.setLayoutManager(new LinearLayoutManager(getActivity().getBaseContext()));

        //linea per separare gli item nella lista
        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);

        rvFoodPlaces.addItemDecoration(itemDecoration);


        return view;
    }





}









