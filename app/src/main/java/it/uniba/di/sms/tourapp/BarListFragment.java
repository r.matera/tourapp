package it.uniba.di.sms.tourapp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class BarListFragment extends Fragment {

    private static final String TAG = "Activity";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.interessi_locali);

    }

    public BarListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragments_list, container, false);


        Log.d(TAG, "onCreate: started.");

        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = view.findViewById(R.id.recyclerv_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), mNames, mImageUrls, mDescription);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        initImageBitmaps();
        initDescriptions();
        return view;
    }

    private void initImageBitmaps() {
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mImageUrls.add("https://media-cdn.tripadvisor.com/media/photo-s/0c/46/55/46/numero-75.jpg");
        mNames.add(getString(R.string.first_bar_name));

        mImageUrls.add("http://media.zonzofox.com/cities/bari/geoplaces/movida/800x400/bari-mozart-caffe.jpg");
        mNames.add(getString(R.string.second_bar_name));



    }

    private void initDescriptions() {
        mDescription.add(getString(R.string.first_bar_description));
        mDescription.add(getString(R.string.second_bar_description));

    }

}
