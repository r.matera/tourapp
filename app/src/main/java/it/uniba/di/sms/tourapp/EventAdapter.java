package it.uniba.di.sms.tourapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ProductViewHolder> {


    //Contesto che useremo per l'inflate del laytou
    private Context mCtx;

    //Immagazziniamo gli eventi un una list
    private List<Event> eventList;

    //Costruttore con contesto ed evento da lista
    public EventAdapter(Context mCtx, List<Event> eventList) {
        this.mCtx = mCtx;
        this.eventList = eventList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //fa inflate e restituisce il nostro viewholder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.layout_events, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        //ottenere l'evento di una posizione specificata
        Event event = eventList.get(position);

        //binding dei dati con le view del viewholder
        holder.textViewTitle.setText(event.getTitle());
        holder.textViewShortDesc.setText(event.getShortdesc());

        holder.imageView.setImageDrawable(mCtx.getResources().getDrawable(event.getImage()));

    }


    @Override
    public int getItemCount() {
        return eventList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textViewShortDesc;
        ImageView imageView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewShortDesc = itemView.findViewById(R.id.textViewShortDesc);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
