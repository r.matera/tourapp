package it.uniba.di.sms.tourapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


public class SleepPlacesFragment extends Fragment {

    ArrayList<SleepPlaces> sleepPlaces;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.menu_sleep);

    }

    public SleepPlacesFragment() {
        // costruttore richiesto
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate del layout
        View view = inflater.inflate(R.layout.fragment_sleep_places, container, false);

        // dichiarazione variabile RecyclerView
        RecyclerView rvHotels = view.findViewById(R.id.rvHotels);

        // Inizializzazione della lista
        sleepPlaces = SleepPlaces.createHotelsList();
        // Creazione adapter passando i dati
        SleepPlacesAdapter adapter = new SleepPlacesAdapter(sleepPlaces);
        // legare l'adapter alla recyclerview per aggiungere i dati
        rvHotels.setAdapter(adapter);
        // Set del layout per la posizione degli item
        rvHotels.setLayoutManager(new LinearLayoutManager(getActivity().getBaseContext()));

        //linea per separare gli item nella lista
        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);

        rvHotels.addItemDecoration(itemDecoration);

        return view;

    }


}


