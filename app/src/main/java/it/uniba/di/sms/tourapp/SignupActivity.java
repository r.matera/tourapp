package it.uniba.di.sms.tourapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Pattern;

public class SignupActivity extends AppCompatActivity {

    private static final Pattern PASSWORD_PATTERN = Pattern.compile("^" +
            "(?=.*[0-9])" +         //deve contenere almeno un numero
            "(?=.*[a-zA-Z])" +      //deve contere almeno una lettera
            ".{4,}" +               //deve essere lunga almeno 4 caratteri
            "$");


    private EditText editTextUsername;
    private EditText editTextMail;
    private EditText editTextPassword;
    private EditText editTextConfirmationPassword;
    private Button signUp_button;

    private TextInputLayout til_name;
    private TextInputLayout til_email;
    private TextInputLayout til_password;
    private TextInputLayout til_password_confirmation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        editTextUsername = findViewById(R.id.input_name);
        editTextMail = findViewById(R.id.input_email);
        editTextPassword = findViewById(R.id.input_password);
        editTextConfirmationPassword = findViewById(R.id.input_control_password);
        signUp_button = findViewById(R.id.signup_button);

        til_name = findViewById(R.id.til_name);
        til_email = findViewById(R.id.til_email);
        til_password = findViewById(R.id.til_password);
        til_password_confirmation = findViewById(R.id.til_password_confirmation);

        signUp_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controlSignUp();
            }
        });
    }


    private void controlSignUp() {
        //controllo su password e email

        editTextUsername.setError(null);
        editTextMail.setError(null);
        editTextPassword.setError(null);
        editTextConfirmationPassword.setError(null);

        String username = editTextUsername.getText().toString();
        String mail = editTextMail.getText().toString();
        String password = editTextPassword.getText().toString();
        String confirmationPassword = editTextConfirmationPassword.getText().toString();

        boolean control = false;


        if(username.isEmpty()) {
            til_name.setError(getString(R.string.signUp_empty_field));
            control = true;
        }


        if (mail.isEmpty()) {
            til_email.setError(getString(R.string.signUp_empty_field));
            control = true;
        } else if (!isEmailValid(mail)) {
            til_email.setError(getString(R.string.signUp_invalid_email));
            control = true;
        }


        if(password.isEmpty()) {
            til_password.setError(getString(R.string.signUp_empty_field));
            control = true;
        } else if (!PASSWORD_PATTERN.matcher(password).matches()) {
            til_password.setError(getString(R.string.signUp_invalid_password));
            control = true;
        }


        if (!(password.equals(confirmationPassword))) {
            til_password_confirmation.setError(getString(R.string.signUp_error_match_password));
            control = true;
        }


        if (control == false) {

            SignUpTask signUpTask = new SignUpTask();
            signUpTask.execute();
        }

    }
    private boolean isEmailValid(String mail) {
        return mail.contains("@");
    }


    private class SignUpTask extends AsyncTask<String, Void, String> {


        HttpURLConnection conn;
        URL url = null;


        @Override
        protected String doInBackground(String... strings) {
            try {

                // indirizzo url dei file php
                url = new URL("http://tourapp.altervista.org/SignUpUtente.php");

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                //classe HttpURLConnection per inviare e ricevere dati da file php
                conn = (HttpURLConnection)url.openConnection();
                conn.setRequestMethod("POST");

                // metodi setDoInput e setDoOutput per la gestione dei dati inviati e ricevuti
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // aggiunta parametri all'url
                String query = URLEncoder.encode("username","UTF-8")+"="+URLEncoder.encode(editTextUsername.getText().toString() , "UTF-8")
                        +"&&"+URLEncoder.encode("mail","UTF-8")+"="+URLEncoder.encode(editTextMail.getText().toString(),"UTF-8")
                        +"&&"+URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(editTextPassword.getText().toString(),"UTF-8");

                // si apre la connessione per ricevere i dati
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // controllo se la connessione è stata fatta
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // lettura dei dati inviati dal server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    String result = "";
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result += line;
                    }

                    // Passaggio dei dati al metodo onPostExecute
                    return(result);

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String s) {
            if (s != null) {
                if (new Boolean(s)) {


                    Intent intent = new Intent(SignupActivity.this, MenuActivity.class);
                    startActivity(intent);
                    SignupActivity.this.finish();

                    String username = editTextUsername.getText().toString();
                    String password = editTextPassword.getText().toString();

                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("username", username);
                    editor.putString("password", password);
                    editor.putBoolean("isLogged", true);
                    editor.commit();

                }
            }
        }
    }

}
