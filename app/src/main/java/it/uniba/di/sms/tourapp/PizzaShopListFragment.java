package it.uniba.di.sms.tourapp;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class PizzaShopListFragment extends Fragment {

    private static final String TAG = "Activity";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.mangiare_pizzerie);

    }

    public PizzaShopListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragments_list, container, false);


        Log.d(TAG, "onCreate: started.");

        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = view.findViewById(R.id.recyclerv_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), mNames, mImageUrls,mDescription);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        initImageBitmaps();
        initDescriptions();
        return view;
    }

    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mImageUrls.add("http://www.pizzeriadadonato.com/images/gallery/03.jpg");
        mNames.add(getString(R.string.pizza_name1));

        mImageUrls.add("https://s3-media4.fl.yelpcdn.com/bphoto/R8mhlkFTuJy1AB4reTQ_LQ/o.jpg");
        mNames.add(getString(R.string.pizza_name2));

        mImageUrls.add("https://scontent-fco1-1.xx.fbcdn.net/v/t1.0-9/26814692_140139283322880_1462561766112920541_n.jpg?_nc_cat=100&_nc_oc=AQnq8g3iQA40WJKghdM14F3motbtbswqsW69PhnD7f9-1g7-h1rNwhgZcaRd5abj0Xw&_nc_ht=scontent-fco1-1.xx&oh=b6642508ba2871095b04cb5bb51486ab&oe=5E60851C");
        mNames.add(getString(R.string.pizza_name3));


    }

    private void initDescriptions(){
        mDescription.add((getString(R.string.desc_pizza1)));

        mDescription.add((getString(R.string.desc_pizza2)));

        mDescription.add((getString(R.string.desc_pizza3)));


    }

}
