package it.uniba.di.sms.tourapp;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class IceCreamShopListFragment extends Fragment {

    private static final String TAG = "Activity";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.mangiare_gelaterie);

    }

    public IceCreamShopListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragments_list, container, false);


        Log.d(TAG, "onCreate: started.");

        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = view.findViewById(R.id.recyclerv_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), mNames, mImageUrls, mDescription);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        initImageBitmaps();
        initDescriptions();
        return view;
    }

    private void initImageBitmaps() {
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mImageUrls.add("https://i1.wp.com/www.viaggi-lowcost.info/wp-content/uploads/2014/07/yogo-bari.jpg?resize=1083%2C720");
        mNames.add(getString(R.string.icecream_name1));

        mImageUrls.add("https://scontent-fco1-1.xx.fbcdn.net/v/t1.0-9/61318214_10156486305738683_1263169217696366592_n.jpg?_nc_cat=105&_nc_oc=AQltjswlTOu43YLs148n8T5akr0dn1-eMoWCdpUsFTrrUfWEivIF_dw2FgFf-iZf1CU&_nc_ht=scontent-fco1-1.xx&oh=0a9c151dcd8a2898114bf5e454406e33&oe=5E18703C");
        mNames.add(getString(R.string.icecream_name2));

        mImageUrls.add("https://www.gdoweek.it/wp-content/uploads/sites/7/2017/02/sandrino-696x462.jpg");
        mNames.add(getString(R.string.icecream_name3));


    }

    private void initDescriptions() {
        mDescription.add((getString(R.string.desc_ice1)));

        mDescription.add((getString(R.string.desc_ice2)));

        mDescription.add((getString(R.string.desc_ice3)));


    }

}

