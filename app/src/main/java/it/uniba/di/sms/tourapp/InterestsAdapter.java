package it.uniba.di.sms.tourapp;


import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


// Creazione di adapter basico che estende RecyclerView.Adapter
//Il ViewHolder darà accesso alle nostre view
public class InterestsAdapter extends
        RecyclerView.Adapter<InterestsAdapter.ViewHolder> {


    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // L' holder contiene una variabile membro per ogni view che verrà settata
        // quando si fa il render della riga
        public TextView nameTextView; //TextView dell'item
        public ImageView imageView; //ImageView dell'icona
        private Context context;


        public ViewHolder(Context context, View itemView) {
            super(itemView);
            this.nameTextView = (TextView) itemView.findViewById(R.id.interest_name);
            this.imageView = (ImageView) itemView.findViewById(R.id.item_icon);
            // Memorizza il context
            this.context = context;
            // Attacca un click listener all'intera view
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // ottiene la posizione dell'item
            if (position != RecyclerView.NO_POSITION) {// Controlla se un item è stato eliminato, ma l'utente lo ha cliccato prima che la UI lo rimuovese
                // We can access the data within the views
                Toast.makeText(context, nameTextView.getText(), Toast.LENGTH_SHORT).show();
                /* Applicazione dello swich in base alla posizione dell'elemento cliccato per indirizzare
                    il flusso di navigazione verso il fragment desiderato. Lancia eccezione in caso di errore.
                 */
                AppCompatActivity activity = (AppCompatActivity) view.getContext();

                switch (position) {
                    case 0:

                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MonumentListFragment()).addToBackStack(null).commit();
                        break;

                    case 1:

                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ChurchListFragment()).addToBackStack(null).commit();

                        break;

                    case 2:

                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ShoreListFragment()).addToBackStack(null).commit();
                        break;

                    case 3:

                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FunListFragment()).addToBackStack(null).commit();

                        break;

                    case 4:

                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new BarListFragment()).addToBackStack(null).commit();

                        break;


                    default:

                        throw new IllegalStateException("Unexpected value: " + position);
                }



            }


        }
    }


    // Variabile membro per gli interessi
    private List<Interest> mInterests;

    // Passaggio della lista di interessi nel costruttore
    public InterestsAdapter(List<Interest> interests) {
        mInterests = interests;
    }


    // Inflating dell'layout da xml e restituisce  il ViewHolder
    @Override
    public InterestsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate del layout xml personalizzato
        View contactView = inflater.inflate(R.layout.item_interest, parent, false);

        // Restituisce un'istanza dell'holder
        ViewHolder viewHolder = new ViewHolder(context, contactView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(InterestsAdapter.ViewHolder viewHolder, int position) {
        // ottieni un modello di dato basato sulla posizione
        Interest interest = mInterests.get(position);

        // Set item view
        TextView textView = viewHolder.nameTextView;
        textView.setText(interest.getName());

        ImageView imgView = viewHolder.imageView;
        imgView.setImageResource(interest.getIcon());

    }


    // Restituisce il  totale degli item nella lista
    @Override
    public int getItemCount() {
        return mInterests.size();
    }


}
